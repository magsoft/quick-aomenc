#!/bin/sh

if [ $# -eq 0 ]
then
    echo "Usage: ./encode [--callgrind] inputFile outputFile"
    exit 1
fi

if [ "$1" = "--callgrind" ]
then
valgrind --tool=callgrind ./aomenc --arnr-maxframes=0 --enable-restoration=0 --passes=1 --cpu-used=8 -t 32 --tile-columns=4 --tile-rows=2 --num-tile-groups=32 --target-bitrate=1000 --end-usage=vbr --enable-cdef=0 -o $3 $2
else
./aomenc --arnr-maxframes=0 --enable-restoration=0 --passes=1 --cpu-used=8 -t 32 --tile-columns=4 --tile-rows=2 --num-tile-groups=32 --target-bitrate=1000 --end-usage=vbr --enable-cdef=0 -o $2 $1
fi

